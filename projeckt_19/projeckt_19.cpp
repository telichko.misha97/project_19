﻿#include <iostream>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "parant class \n"<<std::endl;
    }
    virtual ~Animal()
    {
       // std::cout << "~destr \n " << std::endl;
    }
};

class Dog : public Animal
{
    void Voice() override
    {
        std::cout << "WOOF! WOF! \n" << std::endl;
    }
};

class Cat : public Animal
{
    void Voice() override
    {
        std::cout << "murrrr murrr \n" << std::endl;
    }
};

class Fish : public Animal
{
    void Voice() override
    {
        std::cout << "blup blup.....\n" << std::endl;
    }
};



int main()
{
    Animal* D = new Dog();
    Animal* C = new Cat();
    Animal* F = new Fish();

    Animal* nArray[] = { D, C, F };
    for (int i = 0; i < 3; i++)
    {
        nArray[i]->Voice();
    }

    delete nArray[0];  delete nArray[1]; delete nArray[2];

    return 0;
}